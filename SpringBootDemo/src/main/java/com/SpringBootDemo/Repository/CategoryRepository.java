package com.SpringBootDemo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SpringBootDemo.entity.CategoryEntity;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
CategoryEntity findOneByCode(String code);
}
