package com.SpringBootDemo.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.SpringBootDemo.api.output.NewOutPut;
import com.SpringBootDemo.dto.NewDTO;
import com.SpringBootDemo.service.Impl.NewService;

@RestController
public class NewAPI {
	@Autowired
	private NewService newService;
	@GetMapping("/new")
	public NewOutPut showNew(@RequestParam(value = "page",required = false/*,defaultValue = "NONE"*/)Integer page,
			                 @RequestParam(value = "limit",required = false /*,defaultValue = "NONE"*/)Integer limit) {
		NewOutPut result = new NewOutPut();
		if (page != null && limit != null) {
		//Cach 2:if (page.equals("NONE") && limit.equals("NONE")) {
		result.setPage(page);
		Pageable pageable = new PageRequest(page -1, limit);
		result.setListResult(newService.findAll(pageable));
		result.setTotalPage((int)Math.ceil((double) (newService.totalItem())/limit));
		}else {
			 result.setListResult(newService.findAll());
		}
		
		return result;
	}
	
	@PostMapping("/new")
	public NewDTO createNew(@RequestBody NewDTO model) {
		return newService.save(model);
	}
	@PutMapping(value = "/new/{id}")
	public NewDTO updateNew(@RequestBody NewDTO model,@PathVariable("id") long id) {
		model.setId(id);
		return newService.save(model);
	}
	@DeleteMapping("/new")
	public void deleteNew(@RequestBody long[] ids) {
		newService.delete(ids);
	}
}
 