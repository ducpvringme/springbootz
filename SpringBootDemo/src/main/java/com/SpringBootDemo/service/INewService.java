package com.SpringBootDemo.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.SpringBootDemo.dto.NewDTO;

public interface INewService {

	NewDTO save(NewDTO newDTO);
	//NewDTO update(NewDTO newDTO);
	void delete(long[] ids);
	List<NewDTO> findAll(Pageable pageable);
	int totalItem();
	List<NewDTO> findAll();
}
