package com.SpringBootDemo.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.SpringBootDemo.Repository.CategoryRepository;
import com.SpringBootDemo.Repository.NewRepository;
import com.SpringBootDemo.converter.NewConverter;
import com.SpringBootDemo.dto.NewDTO;
import com.SpringBootDemo.entity.CategoryEntity;
import com.SpringBootDemo.entity.NewEntity;
import com.SpringBootDemo.service.INewService;
@Service
public class NewService implements INewService{
	@Autowired
	private NewRepository newRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired 
	private NewConverter converter;
	@Override

	public NewDTO save(NewDTO newDTO) {
		NewEntity newEntity = new NewEntity();
		if (newDTO.getId()!= null) {
		NewEntity oldNewEntity = newRepository.getOne(newDTO.getId());
		newEntity = converter.toEntity(newDTO, oldNewEntity);
		}else {
			newEntity = converter.toEntity(newDTO);
		}
		CategoryEntity categoryEntity = categoryRepository.findOneByCode(newDTO.getCategoryCode());
		newEntity.setCategory(categoryEntity);
		newEntity = newRepository.save(newEntity);
		return  converter.toDTO(newEntity);
	}
	/*@Override
	public NewDTO update(NewDTO newDTO) {
		NewEntity oldNewEntity = newRepository.findOne(newDTO.getId());
		NewEntity newEntity = converter.toEntity(newDTO, oldNewEntity);
		CategoryEntity categoryEntity = categoryRepository.findOneByCode(newDTO.getCategoryCode());
		newEntity.setCategory(categoryEntity);
		newEntity = newRepository.save(newEntity);

		return converter.toDTO(newEntity);
	}*/
	@Override
	public void delete(long[] ids) {
		for (long item : ids) {
			newRepository.delete(item);
		}
	}
	@Override
	public List<NewDTO> findAll(Pageable pageable) {
		List<NewDTO> result = new ArrayList<>();
		List<NewEntity> entities = newRepository.findAll(pageable).getContent();
		for (NewEntity item : entities) {
			NewDTO newDTO = converter.toDTO(item);
			result.add(newDTO);
		}
		return result;
	}
	@Override
	public int totalItem() {
		return (int) newRepository.count();
	}
	@Override
	public List<NewDTO> findAll() {
		List<NewDTO> result = new ArrayList<>();
		List<NewEntity> entities = newRepository.findAll();
		for (NewEntity item : entities) {
			NewDTO newDTO = converter.toDTO(item);
			result.add(newDTO);
		}
		return result;
	}
}
